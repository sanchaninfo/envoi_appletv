/***
 **Module Name:  PlayerEndViewController.
 **File Name :  PlayerEndViewController.swift
 **Project :   Envoi
 **Copyright(c) : Envoi.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : Next asset Details.
 */

import UIKit

protocol playerEndDelegate:class{
    func playEnd(userId: String, videoId: String, deviceId: String, MyList: Bool,isfromplayend:Bool,storeData:[[String:Any]],DonateData:[[String:Any]])
}

class PlayerEndViewController: UIViewController {
    
    var nextCollectionList = NSMutableArray()
    var nextrowID = Int()
    var userID = String()
    var deviceID = String()
    var nextData = NSDictionary()
    var videoDict = NSDictionary()
    var DonateData = [[String:Any]]()
    var storeData = [[String:Any]]()
    

    @IBOutlet weak var bgImg: UIImageView!
  
    @IBOutlet weak var titleLbl: UILabel!
   
    @IBOutlet weak var automated: UILabel!
    @IBOutlet weak var desLbl: UILabel!
    @IBOutlet weak var thumbImg: UIImageView!

    @IBOutlet weak var duration: UILabel!
    var myTimer = Timer()
    var i = Int()
    var playEnddelegate:playerEndDelegate?
    var videoUrl = String()
    var carouselName = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        i = 5
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleMenuPress))
        tapGesture.allowedPressTypes = [NSNumber(value: UIPressType.menu.rawValue)]
        self.view.addGestureRecognizer(tapGesture)
   
        let metaData = nextData[kMetadata] as! NSDictionary
        let bgImage = isnil(json: metaData, key: "main_carousel_image_url")
        let thumbImage = isnil(json: metaData, key: "movie_art")
        bgImg.kf.setImage(with: URL(string: bgImage))
        thumbImg.kf.setImage(with: URL(string: thumbImage))
        titleLbl.text = isnil(json: nextData, key: "name")
        desLbl.text = isnil(json: nextData, key: "description")
      //  let dur = isnil(json: nextData, key: "file_duration")
        duration.text = "0 m"
      //  duration.text = stringFromTimeInterval(interval: Double(dur)!)
    }

    func handleMenuPress()
    {
        for viewcontroller in (self.navigationController?.viewControllers)!
        {
            if viewcontroller.isKind(of: DetailPageViewController.self)
            {
                 let  _ =  self.navigationController?.popToViewController(viewcontroller, animated: false)
            }
        }
    }
    func getData(getnextData:NSDictionary)
    {
        self.nextData = getnextData
  
        RecentlyWatched(withurl:kRecentlyWatchedUrl)
        myTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.getplay), userInfo: nil, repeats: true)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        i = 5
    }
    func getplay()
    {
   
        automated.text = "\(String(i)) SEC"
        i = i-1
        if i == 0
        {
          myTimer.invalidate()
          print("player is ready")
          playvideo()
        }
        
    }
    
    @IBAction func playButton(_ sender: Any) {
        myTimer.invalidate()
        playvideo()
    }
    
    func playvideo()
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let PlayerVC = storyBoard.instantiateViewController(withIdentifier: "playerLayer") as! PlayerLayerViewController
        var monotize = String()
        var monotizeLabel = String()
        var montizetype = String()
  
        if nextData.object(forKey: "metadata") != nil
        {
            let metaDict = nextData.object(forKey: "metadata") as! NSDictionary
            if metaDict["monetize"] != nil && metaDict["monetize_label"] != nil && metaDict["monetize_type"] != nil
            {
                monotize = metaDict["monetize"] as! String
                monotizeLabel = metaDict["monetize_label"] as! String
                montizetype = metaDict["monetize_type"] as! String
            }
            //self.carouselName = (metaDict["carousel_Name"] as! String)

        }
        else
        {
            if nextData["monetize"] != nil && nextData["monetize_label"] != nil && nextData["monetize_type"] != nil
            {
            monotize = nextData["monetize"] as! String
            monotizeLabel = nextData["monetize_label"] as! String
            montizetype = nextData["monetizetype"] as! String
            }
        }
         _ = String()
            PlayerVC.isResume = false
         
            if nextData["url_m3u8"] != nil && nextData["url_m3u8"] as! String != ""
            {
               videoUrl = nextData["url_m3u8"] as! String
             //  vurl = videoUrl
            }
            else
            {
               videoUrl = nextData["url"] as! String
             //  vurl = videoUrl
            }
            PlayerVC.videoUrl = self.videoUrl
            PlayerVC.mainVideoID = nextData["id"] as! String
            PlayerVC.userID = userID
            PlayerVC.deviceID = deviceID
            PlayerVC.videoID = (nextData["id"] as! String)
            PlayerVC.isMyList = false
            PlayerVC.monitizeLbl = monotizeLabel
            PlayerVC.monitizetype = montizetype
            PlayerVC.carouselName = self.carouselName
            if monotize == "true"
            {
            PlayerVC.DonateData = self.DonateData
            PlayerVC.isDonate = true
            }
            else
            {
            PlayerVC.storeData = self.storeData
            PlayerVC.isDonate = false
            }
            PlayerVC.isfromplayend = true
        self.playEnddelegate?.playEnd(userId: self.userID, videoId: nextData["id"] as! String, deviceId: self.deviceID, MyList: false,isfromplayend:true,storeData:self.storeData,DonateData:self.DonateData)
        self.navigationController?.pushViewController(PlayerVC, animated: true)
       //     let _ = self.navigationController?.popViewController(animated: true)
        
    }
    func RecentlyWatched(withurl:String)
    {
        let parameters = ["createRecentlyWatched": ["videoId":nextData["id"] as! String,"userId":userID as AnyObject]]
   
        ApiManager.sharedManager.postDataWithJson(url: withurl, parameters: parameters as [String : [String : AnyObject]]){(responseDict,error,isDone)in
            if error == nil
            {
                let JSON = responseDict
                let dict = JSON as! NSDictionary
                let dicton = dict["recentlyWatched"] as! NSDictionary
                if (dicton["seekTime"] as! Float64) > 0
                {
                  
                }
            }
            else
            {
                print("json error")
//                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
//                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
//                    UIAlertAction in
//                let _ = self.navigationController?.popViewController(animated: true)
//                })
//                alertview.addAction(defaultAction)
//                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
